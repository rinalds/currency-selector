import React, { useState } from "react";
import styled from "styled-components";

import CurrencySelector from "./components/CurrencySelector";

const Container = styled.div`
  padding: 8px;
`;

function App() {
  const [selectedCurrencies, setSelectedCurrencies] = useState(["eur", "usd"]);

  return (
    <div className="App">
      <Container>
        <CurrencySelector
          onSelectedCurrencyChange={setSelectedCurrencies}
          selectedCurrencies={selectedCurrencies}
          currencies={{
            eur: "EUR",
            pln: "PLN",
            gel: "GEL",
            dkk: "DKK",
            czk: "CZK",
            gbp: "GBP",
            sek: "SEK",
            usd: "USD",
            rub: "RUB"
          }}
        />
      </Container>
    </div>
  );
}

export default App;
