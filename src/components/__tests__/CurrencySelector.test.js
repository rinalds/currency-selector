import React from "react";
import { shallow, mount } from "../../enzyme";

import CurrencySelector from "../CurrencySelector";

const currencies = {
  eur: "EUR",
  pln: "PLN",
  gel: "GEL",
  dkk: "DKK",
  czk: "CZK",
  gbp: "GBP",
  sek: "SEK",
  usd: "USD",
  rub: "RUB"
};

describe("CurrencySelector tests", () => {
  it("renders currencies", () => {
    const selectedCurrencies = [];

    const wrapper = shallow(
      <CurrencySelector
        onSelectedCurrencyChange={newSelectedCurrencies => {}}
        selectedCurrencies={selectedCurrencies}
        currencies={currencies}
      />
    );

    // Expect the wrapper object to be defined
    expect(wrapper.find("#currencies span")).toHaveLength(
      Object.keys(currencies).length
    );
  });

  it("correctly adds currencies", () => {
    const state = { selectedCurrencies: [] };

    const wrapper = mount(
      <CurrencySelector
        onSelectedCurrencyChange={selectedCurrencies => {
          state.selectedCurrencies = selectedCurrencies;
        }}
        selectedCurrencies={state.selectedCurrencies}
        currencies={currencies}
      />
    );

    const el = wrapper.find("#currencies button").at(1);
    const selectedCurrencyValue = el.prop("value");

    wrapper
      .find("#currencies button")
      .at(1)
      .simulate("click");

    // Expect the wrapper object to be defined
    expect(state.selectedCurrencies).toHaveLength(1);
    expect(state.selectedCurrencies[0]).toEqual(selectedCurrencyValue);

    wrapper.setProps({ selectedCurrencies: state.selectedCurrencies });

    wrapper
      .find("#currencies button")
      .at(3)
      .simulate("click");

    // Expect the wrapper object to be defined
    expect(state.selectedCurrencies).toEqual(["pln", "dkk"]);
  });

  it("correctly removes currencies", () => {
    const state = { selectedCurrencies: ["pln", "dkk", "gbp"] };

    const wrapper = mount(
      <CurrencySelector
        onSelectedCurrencyChange={selectedCurrencies => {
          state.selectedCurrencies = selectedCurrencies;
        }}
        selectedCurrencies={state.selectedCurrencies}
        currencies={currencies}
      />
    );

    wrapper
      .find("#currencies button")
      .at(3)
      .simulate("click");

    // Expect the wrapper object to be defined
    expect(state.selectedCurrencies).toEqual(["pln", "gbp"]);
  });

  it("correctly removes currencies when clicking on selected currency", () => {
    const state = { selectedCurrencies: ["pln", "dkk", "gbp"] };

    const wrapper = mount(
      <CurrencySelector
        onSelectedCurrencyChange={selectedCurrencies => {
          state.selectedCurrencies = selectedCurrencies;
        }}
        selectedCurrencies={state.selectedCurrencies}
        currencies={currencies}
      />
    );

    wrapper
      .find("#selected_currencies button")
      .at(1)
      .simulate("click");

    // Expect the wrapper object to be defined
    expect(state.selectedCurrencies).toEqual(["pln", "gbp"]);
  });
});
