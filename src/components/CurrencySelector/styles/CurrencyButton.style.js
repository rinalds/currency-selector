import styled from "styled-components";

const CurrencyButton = styled.div`
  background: transparent;
  border-radius: 3px;
  border: 2px solid ${props => props.theme.main};
  color: ${props => props.theme.main};
  padding: 0.25em 1em;
  position: relative;
  box-sizing: border-box;

  display: inline-flex;
  align-items: center;
  padding: 4px 8px;
  margin: 0 4px 8px 4px;
  width: 68px;

  font-family: system-ui;
  font-size: 11px;

  transition: 0.3s all;
  outline: none;
  background: ${props => (props.selected ? "#FFF" : props.theme.main + "1a")};

  &:hover {
    background: ${props => props.theme.main + "33"};
  }

  &:focus {
    box-shadow: 0 0 0px 1px #db7092;
  }
`;

export default CurrencyButton;
