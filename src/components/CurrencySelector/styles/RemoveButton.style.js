import styled from "styled-components";

const Remove = styled.button`
  border: 2px solid ${props => props.theme.main};
  background: ${props => props.theme.main};
  border-radius: 50%;
  width: 18px;
  height: 18px;
  position: absolute;
  color: white;
  right: -9px;
  top: -9px;
  cursor: pointer;
  transition: 0.2s all;
  outline: none;

  &:after {
    content: "X";
    display: block;
    position: relative;
    margin-left: -3px;
    margin-top: -1px;
    font-family: monospace;
  }
  &:hover {
    background: white;
    &:after {
      color: ${props => props.theme.main};
    }
  }

  &:focus {
    box-shadow: 0 0 0px 1px #db7092;
  }
`;

export default Remove;
