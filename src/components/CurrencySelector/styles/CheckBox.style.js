import styled from "styled-components";

const CheckBox = styled.div`
  border: 1px solid ${props => props.theme.main};
  background: white;
  border-radius: 2px;
  width: 16px;
  height: 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 4px;

  &:after {
    content: "${props => (props.selected ? "x" : "")}";
    display: block;
	font-size: 16px;
	margin-top: -2px;
	transition: 0.2s all;
  }
`;

export default CheckBox;
