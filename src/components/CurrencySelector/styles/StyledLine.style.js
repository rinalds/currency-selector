import styled from "styled-components";

const StyledLine = styled.div`
  height: 1px;
  background: ${props => props.theme.main};
  margin: 0 0 8px 0;
`;

export default StyledLine;
