import styled from "styled-components";

const CurrencyContainer = styled.div`
  display: flex;
  flex-flow: wrap;
`;

export default CurrencyContainer;
