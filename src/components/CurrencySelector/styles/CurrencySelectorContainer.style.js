import styled from "styled-components";

const CurrencySelectorContainer = styled.div`
  width: 100%;
  max-width: 304px;
  border: 1px solid ${props => props.theme.main};
  border-radius: 4px;
  padding: 8px 4px 0 4px;
  box-shadow: 0px 1px 1px ${props => props.theme.main};
`;

export default CurrencySelectorContainer;
