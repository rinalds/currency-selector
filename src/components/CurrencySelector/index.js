import React from "react";
import PropTypes from "prop-types";
import { ThemeProvider } from "styled-components";

import CurrencySelectorContainer from "./styles/CurrencySelectorContainer.style";
import CurrencyContainer from "./styles/CurrencyContainer.style";
import RemoveButton from "./styles/RemoveButton.style";
import CurrencyButton from "./styles/CurrencyButton.style";
import CheckBox from "./styles/CheckBox.style";
import StyledLine from "./styles/StyledLine.style";

const CurrencySelector = ({
  currencies,
  selectedCurrencies,
  onSelectedCurrencyChange,
  color
}) => {
  const toggleCurrency = value => () => {
    const currencyIndex = selectedCurrencies.indexOf(value);

    if (currencyIndex === -1) {
      onSelectedCurrencyChange([...selectedCurrencies, value]);
    } else {
      const newSelectedCurrencies = selectedCurrencies.filter(
        currency => currency !== value
      );
      onSelectedCurrencyChange(newSelectedCurrencies);
    }
  };

  const removeSelectedCurrency = value => () => {
    const newSelectedCurrencies = selectedCurrencies.filter(
      currency => currency !== value
    );
    onSelectedCurrencyChange(newSelectedCurrencies);
  };

  const renderSelectedCurrency = selectedCurrency => (
    <CurrencyButton key={selectedCurrency}>
      <span>{currencies[selectedCurrency]}</span>
      <RemoveButton onClick={removeSelectedCurrency(selectedCurrency)} />
    </CurrencyButton>
  );

  const renderCurrency = currency => {
    const [value, title] = currency;

    return (
      <CurrencyButton
        value={value}
        as="button"
        key={value}
        selected={selectedCurrencies.indexOf(value) > -1}
        onClick={toggleCurrency(value)}
      >
        <CheckBox selected={selectedCurrencies.indexOf(value) > -1} />
        <span>{title}</span>
      </CurrencyButton>
    );
  };

  return (
    <ThemeProvider theme={{ main: color }}>
      <CurrencySelectorContainer>
        <CurrencyContainer id="selected_currencies">
          {selectedCurrencies.map(renderSelectedCurrency)}
        </CurrencyContainer>
        {selectedCurrencies.length > 0 && <StyledLine />}
        <CurrencyContainer id="currencies">
          {Object.entries(currencies).map(renderCurrency)}
        </CurrencyContainer>
      </CurrencySelectorContainer>
    </ThemeProvider>
  );
};

CurrencySelector.propTypes = {
  currencies: PropTypes.object.isRequired,
  selectedCurrencies: PropTypes.array.isRequired,
  onSelectedCurrencyChange: PropTypes.func.isRequired,
  color: PropTypes.string
};

CurrencySelector.defaultProps = {
  color: "#7b5e67"
};

export default CurrencySelector;
