import React, { useState } from "react";
import { action } from "@storybook/addon-actions";
import CurrencySelector from ".";

export default {
  title: "CurrencySelector",
  component: CurrencySelector
};

export const Default = () => {
  const [selectedCurrencies, setSelectedCurrencies] = useState(["eur", "usd"]);

  return (
    <CurrencySelector
      onSelectedCurrencyChange={newSelectedCurrencies => {
        action("onSelectedCurrencyChange");
        setSelectedCurrencies(newSelectedCurrencies);
      }}
      selectedCurrencies={selectedCurrencies}
      currencies={{
        eur: "EUR",
        pln: "PLN",
        gel: "GEL",
        dkk: "DKK",
        czk: "CZK",
        gbp: "GBP",
        sek: "SEK",
        usd: "USD",
        rub: "RUB"
      }}
    />
  );
};
